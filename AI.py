import random
import socket

HOST = "127.0.0.1"
PORT = 1234

black_figure = ["p", "r", "n", "b", "q", "k"]
fields = ["a", "b", "c", "d", "e", "f", "g", "h"]
price_white_figure = [10, 30, 30, 50, 90, 900]
price_black_figure = [-i for i in price_white_figure]


def get_figure(write_file, read_file) -> str:
    while True:
        letter = random.choice(fields)
        num = str(random.randint(1, 8))
        write_file.write(f"get {letter + num}" + " \n")
        write_file.flush()
        data = read_file.readline().strip()
        for figure in black_figure:
            if figure in data:
                return letter + num


def move(write_file, read_file):
    while True:
        start_position = get_figure(write_file, read_file)
        for i in range(64):
            letter = random.choice(fields)
            num = str(random.randint(1, 8))
            write_file.write(f"move {start_position} {letter + num}" + " \n")
            write_file.flush()
            data = read_file.readline().strip()
            if data == "success":
                print(f"move {start_position} {letter + num}")


def client():
    HOST = "127.0.0.1"
    PORT = 8080
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        read_file = s.makefile(mode="r", encoding="utf-8")
        write_file = s.makefile(mode="w", encoding="utf-8")
        count = 0
        while True:
            data = read_file.readline().strip()
            print(data)
            if "/." in data:
                data = data[8:] + "/"
                result = []
                line = []
                for i in range(64 + 8):
                    if data[i] != "/":
                        line.append(data[i])
                    else:
                        result.append(line)
                        line = []

                for i in result:
                    print(i)
            if count == 0:
                count += 1
                write_file.write("connect_to_room 1" + " \n")
                write_file.flush()
            elif count == 1:
                count += 1
                write_file.write("start_game" + " \n")
                write_file.flush()
            else:
                move(write_file, read_file)


if __name__ == "__main__":
    client()
