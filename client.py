import socket
from beautifultable import BeautifulTable

HOST = "127.0.0.1"
PORT = 1234
table = BeautifulTable()
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    read_file = s.makefile(mode="r", encoding="utf-8")
    write_file = s.makefile(mode="w", encoding="utf-8")
    count = 0
    while True:
        c = 9
        data = read_file.readline().strip()
        print(data)
        if "/." in data:
            data = data[8:] + "/"
            result = []
            line = []
            for i in range(64 + 8):
                if data[i] != "/":
                    line.append(data[i])
                else:
                    result.append(line)
                    line = []
            print(*['###', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '###'])
            print('--' * 11 + '-')
            for i in result:
                c -= 1
                print(c, '|', *i, '|', c)
            print('--' * 11 + '-')
            print(*['###', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '###'])
            #     print("connect_to_room 123\n")
            #     print("start_game\n")
            #     print("board" + " \n")

            # else:
        if count == 0:
            count += 1
            write_file.write("connect_to_room 123" + " \n")
            write_file.flush()
        elif count == 1:
            count += 1
            write_file.write("start_game" + " \n")
            write_file.flush()
        else:
            a = input("send: ")
            if not a:
                break
            # if "move" in a:
            write_file.write(a + " \n")

            write_file.flush()
    s.close()
